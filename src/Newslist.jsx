export const newsList = [
    {
        id: 1,
        name: "News1",
        data: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolore ea ab asperiores nulla autem, voluptates nostrum debitis culpa, itaque quibusdam quia vero dolores earum perspiciatis accusantium, aut porro dolorum officia.",
        img: "https://reform.by/wp-content/uploads/2021/05/42.jpg"
    },

    {
        id: 2,
        name: "News2",
        data: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolore ea ab asperiores nulla autem, voluptates nostrum debitis culpa, itaque quibusdam quia vero dolores earum perspiciatis accusantium, aut porro dolorum officia.",
        img: "https://cdn.turkishairlines.com/m/538ee35cf95e10a6/original/Travel-Guide-of-Minsk-via-Turkish-Airlines.jpg"
    },

    {
        id: 3,
        name: "News3",
        data: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolore ea ab asperiores nulla autem, voluptates nostrum debitis culpa, itaque quibusdam quia vero dolores earum perspiciatis accusantium, aut porro dolorum officia.",
        img: "https://cs10.pikabu.ru/post_img/big/2018/11/10/5/1541830376165623747.jpg"
    },

    {
        id: 4,
        name: "News4",
        data: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolore ea ab asperiores nulla autem, voluptates nostrum debitis culpa, itaque quibusdam quia vero dolores earum perspiciatis accusantium, aut porro dolorum officia.",
        img: "https://i.c97.org/ai/430432/aux-small-1626897253-20210721_kloun_360.jpg"
    }
]
