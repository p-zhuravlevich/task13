import React, {Component} from 'react';
import Button from "./Button";
import {newsList} from './Newslist';

class News extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            news: newsList,
            value: ''
        }
    }

    handleChange = (event) => {
        this.setState({value: event.target.value});
    }

    clear = () => {
        this.setState({
           news: newsList
        })
    }

    findNews = () => {
        this.setState({
            news: this.state.news.filter(el => {
                return el.name.toLowerCase() === this.state.value.toLocaleLowerCase();
            })
        })
    }

    filterNews = () => {
        this.setState({
            news: this.state.news.reverse()
        })
    }


    render() {

        return (
            <React.Fragment>
                <h1>Task 13 - News</h1>
                <div>
                    <label>
                        <button onClick={this.clear}>Clear</button>
                        <input type="text" value={this.state.value} onChange={this.handleChange} />
                        <button onClick={this.findNews}>Find</button>
                        <button onClick={this.filterNews}>Filter</button>
                    </label>
                </div>
                {this.state.news.map((post, i) => i === 0 ?
                 <Button key={post.id} post={post} i={i} flag/> :
                  <Button key={post.id} post={post} i={i}/>)}
            </React.Fragment>
        )
    }
}

export default News;
