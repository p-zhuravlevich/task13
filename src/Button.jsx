import React, {Component} from 'react';


class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showContent: this.props.flag
        }
    }
    
    componentWillReceiveProps(nextProps, nextState) {
        if(nextProps.flag !== this.state.showContent){
            this.setState({
                showContent: nextProps.flag
            })
        }
    }

    render() {
        const data = (
            <p>{this.props.post.data}</p>
        );

        return (
            <React.Fragment>
            <div>
                <h1>{this.props.post.name}</h1>
                <div><img src={this.props.post.img} alt={this.props.post.name}/></div>
                <button onClick={this.handleClick}>{this.state.showContent ? "Свернуть" : "Развернуть"}</button>
                {this.state.showContent && data}
            </div>
            
            </React.Fragment>
        )
    }

    handleClick = () => {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }
}

export default Button;
